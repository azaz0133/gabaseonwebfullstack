import React, { Component } from 'react';
import Axios from 'axios'
import './app.css'
class App extends Component {
  state ={
    value:"",
    data:[],
    isLoading:false
  }

   handleSubmit = (e) => {
     e.preventDefault();
     this.setState({
       isLoading:true
     })
      Axios.get(`http://localhost:8080/api/python/${this.state.value}`).then( ({data}) => {
          this.setState({
            data,
            isLoading:false
          })
      })
   }
   

  render() {
    return (
      <div className="box-example" >
       <div style={{textAlign:"center"}}>
       <h1 className="text-focus-in" style={{color:"white",marginTop:"50px"}}>
       Genetic Algorithm
       </h1>

       <form >
          <h1>type some text in english ....</h1>
          <div class="question">
            <input type="text" onChange={e=>this.setState({value:e.target.value})} required/>
            <label>Input</label>
          </div>
          <button onClick={e=>this.handleSubmit(e)}>Submit</button>

          <div style={{textAlign:"center",marginTop:"100px"}}>
          {this.state.isLoading  && 
           <div class="bouncing-loader">
             <div></div>
             <div></div>
             <div></div>
           </div>
         }
          {this.state.data['output'] && this.state.data['output'].map( d => <h3>{d}</h3>)}
        </div>
        </form>
       </div>       
      </div>
    );
  }
}

export default App;
