var express = require('express')
var app = express();
var bodyParser = require('body-parser');
var router = express.Router()
var cors = require('cors')
var python = require('python-shell')

var shell = python.PythonShell
app.use(cors())
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: false }))

app.use('/api/python/:text',(req,res)=>{
    const text = req.params.text
    shell.run('ga_string_guess.py',{
        args:[text]
    },(err,result)=>{
        if(err) console.log(err)
        setTimeout(_=>{
            res.json({
                text,
                length:result.length,
                output:result
            })
        },1000)
    })
})

app.listen(8080,()=>{
    console.log("start on port ",8080)
})